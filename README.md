# The Odin Project

Finished projects of the Odin Project web development course.

[Check out the projects](https://ermy.gitlab.io/the-odin-project)
