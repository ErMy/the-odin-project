const displayController = (() => {
    let grid = [];
    let resultTxt;

    const initialize = () => {
        for (let index = 0; index < 9; index++) {
            grid.push(document.querySelector(`#${CSS.escape(index)}`));
        }
        
        resultTxt = document.querySelector('#result');
    }

    const setField = (index, marker, isCom) => {
        grid[index].textContent = marker;
        if (isCom) {
            grid[index].classList.remove('c-player');
            grid[index].classList.add('c-com');
        } else {
            grid[index].classList.remove('c-com');
            grid[index].classList.add('c-player');
        }
    }

    const setResultText = (txt) => {
        resultTxt.textContent = txt;
    }

    const clearDisplay = () => {
        for (let index = 0; index < grid.length; index++) {
            grid[index].textContent = '';            
        }
        resultTxt.textContent = '';
    }

    return {
        initialize,
        setField,
        setResultText,
        clearDisplay
    };
})();

const gameBoard = (() => {
    let board = [];

    const setField = (index, marker, isCom) => {
        board[index] = marker;
        displayController.setField(index, marker, isCom);
    }

    const getField = (index) => {
        return board[index];
    }

    const getBoardState = () => board;

    const isGameWon = () => {
        if (board[0] == board[1] && board[0] == board[2] && board[0].length > 0
            || board[3] == board[4] && board[3] == board[5] && board[3].length > 0
            || board[6] == board[7] && board[6] == board[8] && board[6].length > 0
            || board[0] == board[3] && board[0] == board[6] && board[0].length > 0
            || board[1] == board[4] && board[1] == board[7] && board[1].length > 0
            || board[2] == board[5] && board[2] == board[8] && board[2].length > 0
            || board[0] == board[4] && board[0] == board[8] && board[0].length > 0
            || board[2] == board[4] && board[2] == board[6] && board[2].length > 0) {
                return true;
            } 
            return false;
    }
    const isGameTied = () => {
        for (let index = 0; index < board.length; index++) {
            if (board[index].length == 0)
                return false;            
        }
        return true;
    }

    const clearBoard = () => {
        board = [];
        initialize();
        displayController.clearDisplay();
    }

    const initialize = () => {
        for (let index = 0; index < 9; index++) {
            board.push('');            
        }
    }

    return {
        getField,
        setField,
        getBoardState,
        isGameWon,
        isGameTied,
        clearBoard,
        initialize
    };
})();

const gameController = (() => {
    let players;
    let activeIndex = 0;

    const start = () => {
        players = [];
        const randVal = Math.random();
        players.push(Player('X', randVal < 0.5));
        players.push(Player('O', randVal >= 0.5));

        initializeComponents();
        updateDisplayMessage();            
        
        players[activeIndex].startTurn();     
    }

    const updateDisplayMessage = () => {
        if (players[activeIndex].isCOM()) {
            displayController.setResultText("");
        } else {
            displayController.setResultText("Your turn");
        }
    }

    const initializeComponents = () => {
        initialize();
        players.forEach(player => {
            player.initialize();
        });
        displayController.initialize();
        gameBoard.initialize();
    }

    const initialize = () => {
        document.querySelector('#restart').addEventListener('click', restart);
    }

    const markField = (index, marker, isCom) => {
        gameBoard.setField(index, marker, isCom);

        if (gameBoard.isGameWon()) {
          if (isCom) {
            displayController.setResultText("You lose!");
          } else {
            displayController.setResultText("You win!");
          }
        } else if (gameBoard.isGameTied()) {
            displayController.setResultText("It's a tie!");
        } else {
          activeIndex = (activeIndex + 1) % 2;

          if (players[activeIndex].isCOM()) {
              displayController.setResultText("");
          } else {
              displayController.setResultText("Your turn");
          }

          players[activeIndex].startTurn();
        }
    }

    const isValidPlay = (index) => {
        if (gameBoard.getField(index) === '') {
            return true;
        } else {
            return false;
        }
    }

    const restart = () => {
        gameBoard.clearBoard();
        shuffleArray(players);
        players[0].setMarker('X');
        players[1].setMarker('O');
        updateDisplayMessage();

        players[activeIndex].startTurn(); 
    }

    return {
        start,
        markField,
        isValidPlay
    };
})();

const Player = (marker, com) => {
    let activeTurn = false;

    const isCOM = () => com;

    const setMarker = (m) => { marker = m; } 

    const startTurn = () => {
        activeTurn = true;
        if (isCOM()) {
            setTimeout(makeTurnCOM, 500);
        }
    }

    // callback for mouse click event
    const makeTurn = (event) => {
        if (activeTurn) {
            const index = Number(event.target.id);
            if (gameController.isValidPlay(index)) {
                activeTurn = false;
                gameController.markField(index, marker, isCOM());
            }
        }
    }

    // get the board and choose field to mark
    const makeTurnCOM = () => {
        const emptyFields = gameBoard.getBoardState().reduce((emptyFields, field, index) => {
            if (field === '') {
                emptyFields.push(index);
            }
            return emptyFields;
        }, []);

        const randIndex = Math.floor(Math.random() * emptyFields.length);

        gameController.markField(emptyFields[randIndex], marker, isCOM());
        activeTurn = false;
    }

    // hook up UI with callback
    const initialize = () => {
        if (isCOM()) {
            return;
        }

        const fields = document.querySelectorAll('.play-field');
        fields.forEach(field => {
            field.addEventListener('click', makeTurn);
        });
    }

    return {
        isCOM,
        startTurn,
        initialize,
        setMarker
    }
};

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

gameController.start();