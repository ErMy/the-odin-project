const rockBtn = document.querySelector("#Rock");
rockBtn.addEventListener("click", play);
const paperBtn = document.querySelector("#Paper");
paperBtn.addEventListener("click", play);
const scissorsBtn = document.querySelector("#Scissors");
scissorsBtn.addEventListener("click", play);

const youScoreTxt = document.querySelector("#you-score");
const tieScoreTxt = document.querySelector("#tie-score");
const comScoreTxt = document.querySelector("#com-score");

const player = document.querySelector("#player-choice").parentElement;
const playerImg = document.querySelector("#player-choice img");
const comPlayer = document.querySelector("#com-choice").parentElement;
const comImg = document.querySelector("#com-choice img");

const resultTxt = document.querySelector("#result");

function play(e) {
  const computerSelection = getComChoice();

  if (this.id == "Rock") {
    playerImg.src = "img/rock.jpg";
  } else if (this.id == "Paper") {
    playerImg.src = "img/paper.jpg";
  } else if (this.id == "Scissors") {
    playerImg.src = "img/scissors.jpg";
  }

  if (computerSelection == "Rock") {
    comImg.src = "img/rock.jpg";
  } else if (computerSelection == "Paper") {
    comImg.src = "img/paper.jpg";
  } else if (computerSelection == "Scissors") {
    comImg.src = "img/scissors.jpg";
  }

  const winner = playRound(this.id, computerSelection);

  if (winner == 0) {
    let parsed = parseInt(youScoreTxt.textContent);
    parsed++;
    youScoreTxt.textContent = parsed.toString();
    player.classList.remove("lose", "tie");
    player.classList.add("win");
    comPlayer.classList.remove("win", "tie");
    comPlayer.classList.add("lose");
    resultTxt.textContent = "You win!";
  } else if (winner == 1) {
    let parsed = parseInt(comScoreTxt.textContent);
    parsed++;
    comScoreTxt.textContent = parsed.toString();
    comPlayer.classList.remove("lose", "tie");
    comPlayer.classList.add("win");
    player.classList.remove("win", "tie");
    player.classList.add("lose");
    resultTxt.textContent = "You lose!";
  } else {
    let parsed = parseInt(tieScoreTxt.textContent);
    parsed++;
    tieScoreTxt.textContent = parsed.toString();
    player.classList.remove("lose", "win");
    player.classList.add("tie");
    comPlayer.classList.remove("lose", "win");
    comPlayer.classList.add("tie");
    resultTxt.textContent = "It's a tie!";
  }
}

function playRound(playerSelection, computerSelection) {
  if (
    (playerSelection == "Rock" && computerSelection == "Paper") ||
    (playerSelection == "Paper" && computerSelection == "Scissors") ||
    (playerSelection == "Scissors" && computerSelection == "Rock")
  ) {
    return 1;
  }

  if (
    (playerSelection == "Rock" && computerSelection == "Rock") ||
    (playerSelection == "Paper" && computerSelection == "Paper") ||
    (playerSelection == "Scissors" && computerSelection == "Scissors")
  ) {
    return -1;
  }

  if (
    (playerSelection == "Paper" && computerSelection == "Rock") ||
    (playerSelection == "Scissors" && computerSelection == "Paper") ||
    (playerSelection == "Rock" && computerSelection == "Scissors")
  ) {
    return 0;
  }
}

function getComChoice() {
  let choices = ["Rock", "Paper", "Scissors"];
  return choices[Math.floor(Math.random() * choices.length)];
}
