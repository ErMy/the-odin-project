let num1 = null;
let num2 = null;
let operator = null;

let displayTerm = document.querySelector("#display_term");
let displayNum = document.querySelector("#display_number");

document.querySelector("#zero").addEventListener("click", btn_zero);
document.querySelector("#one").addEventListener("click", btn_one);
document.querySelector("#two").addEventListener("click", btn_two);
document.querySelector("#three").addEventListener("click", btn_three);
document.querySelector("#four").addEventListener("click", btn_four);
document.querySelector("#five").addEventListener("click", btn_five);
document.querySelector("#six").addEventListener("click", btn_six);
document.querySelector("#seven").addEventListener("click", btn_seven);
document.querySelector("#eight").addEventListener("click", btn_eight);
document.querySelector("#nine").addEventListener("click", btn_nine);
document.querySelector("#equal").addEventListener("click", equals);
document.querySelector("#plus").addEventListener("click", btn_plus);
document.querySelector("#minus").addEventListener("click", btn_minus);
document.querySelector("#multiply").addEventListener("click", btn_multiply);
document.querySelector("#divide").addEventListener("click", btn_divide);
document.querySelector("#sign").addEventListener("click", btn_sign);
document.querySelector("#decimal").addEventListener("click", btn_decimal);
document.querySelector("#clear").addEventListener("click", btn_clear);
document.querySelector("#back").addEventListener("click", backspace);

function add(a, b) {
  return a + b;
}

function subtract(a, b) {
  return a - b;
}

function multiply(a, b) {
  return a * b;
}

function divide(a, b) {
  return a / b;
}

function operate(a, b, op) {
  switch (op) {
    case "+":
      return add(a, b);
    case "-":
      return subtract(a, b);
    case "*":
      return multiply(a, b);
    case "/":
      return divide(a, b);
  }
}

function getDisplayText() {
  return displayNum.textContent;
}

function setDisplayText(txt) {
  displayNum.textContent = txt;
}

function setTermText(txt) {
  displayTerm.textContent = txt;
}

function equals() {
  if (num1 && operator) {
    if (!num2) {
      num2 = getDisplayText();
    }

    setTermText(num1 + " " + operator + " " + num2 + " = ");
    num1 = operate(Number(num1), Number(num2), operator).toString();
    setDisplayText(num1);
    clear();
  }
}

function clear() {
  num1 = null;
  num2 = null;
  operator = null;
}

function btn_clear() {
  clear();
  setDisplayText("0");
}

function backspace() {
  if (num2) {
    if (num2.length > 1) {
      num2 = num2.slice(0, num2.length - 1);
      setDisplayText(num2);
    } else {
      num2 = null;
      setDisplayText('0');
    }
  } else if (num1 && !operator) {
    if (num1.length > 1) {
      num1 = num1.slice(0, num1.length - 1);
      setDisplayText(num1);
    } else {
      num1 = null;
      setDisplayText('0');
    }
  }
}

function btn_sign() {
  if (operator == null) {
    if (num1 == null) {
      num1 = getDisplayText();
    }

    if (num1.charAt(0) == "-") {
      num1 = num1.slice(1);
      setDisplayText(num1);
    } else {
      num1 = "-" + num1;
      setDisplayText(num1);
    }
  } else if (num2 == null) {
    num2 = "-" + num1;
    setDisplayText(num2);
  } else {
    if (num2.charAt(0) == "-") {
      num2 = num2.slice(1);
      setDisplayText(num2);
    } else {
      num2 = "-" + num2;
      setDisplayText(num2);
    }
  }
}

function btn_decimal() {
  if (num1 == null) {
    num1 = "0.";
    setDisplayText(num1);
  }
  if (operator == null) {
    num1 = num1.indexOf(".") == -1 ? num1 + "." : num1;
    setDisplayText(num1);
  } else if (num2 == null) {
    num2 = '0.';
    setDisplayText(num2);
  } else {
    num2 = num2.indexOf(".") == -1 ? num2 + "." : num2;
    setDisplayText(num2);
  }
}


function btn_zero() {
  if (operator == null) {
    num1 = num1 ? num1 + "0" : "0";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "0" : "0";
    setDisplayText(num2);
  }
}

function btn_one() {
  if (operator == null) {
    num1 = num1 ? num1 + "1" : "1";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "1" : "1";
    setDisplayText(num2);
  }
}

function btn_two() {
  if (operator == null) {
    num1 = num1 ? num1 + "2" : "2";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "2" : "2";
    setDisplayText(num2);
  }
}

function btn_three() {
  if (operator == null) {
    num1 = num1 ? num1 + "3" : "3";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "3" : "3";
    setDisplayText(num2);
  }
}

function btn_four() {
  if (operator == null) {
    num1 = num1 ? num1 + "4" : "4";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "4" : "4";
    setDisplayText(num2);
  }
}

function btn_five() {
  if (operator == null) {
    num1 = num1 ? num1 + "5" : "5";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "5" : "5";
    setDisplayText(num2);
  }
}

function btn_six() {
  if (operator == null) {
    num1 = num1 ? num1 + "6" : "6";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "6" : "6";
    setDisplayText(num2);
  }
}

function btn_seven() {
  if (operator == null) {
    num1 = num1 ? num1 + "7" : "7";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "7" : "7";
    setDisplayText(num2);
  }
}

function btn_eight() {
  if (operator == null) {
    num1 = num1 ? num1 + "8" : "8";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "8" : "8";
    setDisplayText(num2);
  }
}

function btn_nine() {
  if (operator == null) {
    num1 = num1 ? num1 + "9" : "9";
    setDisplayText(num1);
    setTermText("");
  } else {
    num2 = num2 ? num2 + "9" : "9";
    setDisplayText(num2);
  }
}


function btn_plus() {
  operator = "+";

  if (num1 == null) {
    num1 = getDisplayText();
    setTermText(num1 + " " + operator + " ");
  } else if (num2 != null) {
    equals();
  } else {
    setTermText(num1 + " " + operator + " ");
  }
}

function btn_minus() {
  operator = "-";

  if (num1 == null) {
    num1 = getDisplayText();
    setTermText(num1 + " " + operator + " ");
  } else if (num2 != null) {
    equals();
  } else {
    setTermText(num1 + " " + operator + " ");
  }
}

function btn_multiply() {
  operator = "*";

  if (num1 == null) {
    num1 = getDisplayText();
    setTermText(num1 + " " + operator + " ");
  } else if (num2 != null) {
    equals();
  } else {
    setTermText(num1 + " " + operator + " ");
  }
}

function btn_divide() {
  operator = "/";

  if (num1 == null) {
    num1 = getDisplayText();
    setTermText(num1 + " " + operator + " ");
  } else if (num2 != null) {
    equals();
  } else {
    setTermText(num1 + " " + operator + " ");
  }
}

