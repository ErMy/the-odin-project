let gridSize = 60;
let mouseDown = false;

const sketchArea = document.querySelector("#sketch-area");
const clearBtn = document.querySelector('#clear_btn');
clearBtn.addEventListener('click', onClearBtnClick);
document.onmousedown = () => {mouseDown = true;};
document.onmouseup = () => {mouseDown = false;};

createGrid();

function createGrid() {
  sketchArea.style.gridTemplateColumns = `repeat(${gridSize}, 1fr)`;

  for (let i = 0; i < gridSize * gridSize; i++) {
    const gridElement = document.createElement("div");
    gridElement.classList.add("grid-square");

    gridElement.addEventListener("mouseover", (e) => {
      if (mouseDown) {
        e.target.classList.add("bg-color");
      }
    });

    sketchArea.appendChild(gridElement);
  }
}

function removeGrid() {
    while (sketchArea.lastChild) {
        sketchArea.removeChild(sketchArea.lastChild);
    }
}

function onClearBtnClick() {
    promptNewSize();
    removeGrid();
    createGrid();
}

function promptNewSize(){
    let size = prompt("New grid size:");
    size = parseInt(size, 10);
    if (isNaN(size) || size < 1 || size > 100) {
        return;
    }
    gridSize = size;
}