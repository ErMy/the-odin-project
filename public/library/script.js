let myLibrary = [new Book("Harry Potter und der Stein der Weisen", "J.K. Rowling", 336, true), 
                new Book("Der Name des Windes", "Patrick Rothfuss", 865, false),
                new Book("Ascendance of a Bookworm Vol. 01", "Miya Kazuki", 261, true)];

function Book(title, author, pageCount, read) {
    this.title = title;
    this.author = author;
    this.pageCount = pageCount;
    this.read = read;
}

function addBookToLibrary(event) {
    event.preventDefault();

    if (titleField.value.length > 0 && authorField.value.length > 0) {
        let book = new Book(titleField.value, authorField.value, Number(pagesField.value), readCheckbox.checked);
        myLibrary.push(book);
        addBookToTable(book);
    }
}

function addBookToTable(book){
    let row = table.insertRow();
    let cell1 = row.insertCell(0);
    let cell2 = row.insertCell(1);
    let cell3 = row.insertCell(2);
    let cell4 = row.insertCell(3);
    let cell5 = row.insertCell(4);

    cell1.textContent = book.title;
    cell2.textContent = book.author;
    cell3.textContent = book.pageCount;

    let checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.checked = book.read;
    checkbox.addEventListener('click', function() {
        book.read = !book.read;
    });
    cell4.appendChild(checkbox);

    let remove = createRemoveButton(book);
    cell5.appendChild(remove);
}

function displayLibrary() {
    for (let index = 0; index < myLibrary.length; index++) {
        const book = myLibrary[index];
        addBookToTable(book);
    }
}

function createRemoveButton(book) {
    let anchor = document.createElement('a');
    let img = document.createElement('img');
    img.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/OOjs_UI_icon_trash_apex.svg/1024px-OOjs_UI_icon_trash_apex.svg.png";
    anchor.addEventListener('click', function() {
         const i = myLibrary.findIndex(x => x == book);
         myLibrary.splice(i, 1);
         table.deleteRow(i+1);
    });
    anchor.appendChild(img);
    return anchor;
}


const titleField = document.querySelector("#book-title");
const authorField = document.querySelector("#book-author");
const pagesField = document.querySelector("#book-pages");
const readCheckbox = document.querySelector("#book-read");
const table = document.querySelector(".library-table");
document.querySelector("form").addEventListener('submit', addBookToLibrary);

displayLibrary();